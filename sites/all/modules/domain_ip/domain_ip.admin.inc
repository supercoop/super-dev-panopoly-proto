<?php

/**
 * @file
 * Page callback file for the ip ranges module.
 */

/**
 * Menu callback. Displays banned IP ranges.
 */
function domain_ip_restriction_page($domain) {
  $rows = array();
  $header = array(
    t('IP addresses'),
    t('list type'),
    t('Owner'),
    t('Operations'));
  $domain_id = $domain['domain_id'];
  $ip_list = domain_ip_get_ip_list($domain_id);
  foreach ($ip_list as $ip) {
    $rows[] = array(
      str_replace('-', ' - ', $ip->ip),
      $ip->type,
      check_plain($ip->author),
      l(t('delete'), "admin/structure/domain/view/$domain_id/ip/delete/$ip->bid/$ip->ip"),
    );
  }

  $build['domain_ip_form'] = drupal_get_form('domain_ip_form', $domain);

  $build['domain_ip_banning_table'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
  );

  return $build;
}

/**
 * Defines the form for banning IP addresses.
 *
 * @ingroup forms
 * @see domain_ip_form_submit()
 */
function domain_ip_form($form, &$form_state, $domain) {
  $form['domain'] = array(
    '#type' => 'value',
    '#value' => $domain,
  );
  $domain_id = $domain['domain_id'];
  $domain_name = $domain['subdomain'];
  $list = domain_ip_get_domain_list($domain_id);
  if ($list == 'Blacklist' || $list == NULL) {
    $list_message = t('<strong>You are currently using Blacklist. All ips in Blacklist will be banned,
      even if the ip is also in Whitelist.<br /> Before switching to whitelist, make sure
      you have clicked below link to whitelist your own ip-address,
      or you will be banned from %domain_name immediately!</strong><br />', array(
        '%domain_name' => $domain_name));
  }
  else {
    $list_message = t('<strong>You are currently on the Whitelist. Only whitelisted IP address
    can access the intranet domain. All other IP addresses (whether or not they are explicitly added to the blacklist)
    will be denied access.</strong><br />');
  }
  $form['message'] = array(
    '#markup' => l(t('Click here to manage error message showed to banned user.'),
      'admin/structure/domain/view/' . $domain['domain_id'] . '/ip/message') . '<br />',
  );
  $form['list'] = array(
    '#markup' => $list_message . l(t('Click here to switch list.'),
      'admin/structure/domain/view/' . $domain['domain_id'] . '/ip/switchlist') . '</strong><br />',
  );
  $form['ip_start'] = array(
    '#prefix' => t('<strong>Note that your own IP-Address is currently <i> %ip </i> Be carefull not to lock yourself out!</strong><br />', array('%ip' => ip_address())) . l(t('Click here to whitelist your own IP-address.'),
    'admin/structure/domain/view/' . $domain['domain_id'] . '/ip/whitelist_own'),
    '#title' => t('IP range start / Single IP-address'),
    '#type' => 'textfield',
    '#size' => 48,
    '#required' => TRUE,
    '#maxlength' => 15,
    '#description' => t('Enter IP-address (100.100.100.100). If range end is specified, it will be used as start of the range, otherwise as a single IP-address.'),
  );
  $form['ip_end'] = array(
    '#title' => t('IP range end (optional)'),
    '#type' => 'textfield',
    '#size' => 48,
    '#required' => FALSE,
    '#maxlength' => 15,
    '#description' => t('If entered, the banned ip will be treated as a range.'),
  );
  $form['type'] = array(
    '#title' => t('List type'),
    '#type' => 'select',
    '#multiple' => FALSE,
    '#options' => array('blacklist' => 'blacklist', 'whitelist' => 'whitelist'),
    '#default_value' => 'blacklist',
    '#required' => TRUE,
    '#description' => t('Choose list type.'),
  );
  $form['author'] = array(
    '#title' => t('Owner'),
    '#type' => 'textfield',
    '#size' => 30,
    '#required' => TRUE,
    '#maxlength' => 15,
    '#description' => t('Owner of this list.'),
  );
  if (isset($form_state['flag']) && $form_state['flag']) {
    $form['black_sign'] = array(
      '#type' => 'checkbox',
      '#title' => t('Yes. I want to add my own ip into Blacklist.'),
    );
  }
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Add'),
  );
  return $form;
}

/**
 * Form validation handler for domain_ip_form().
 */
function domain_ip_form_validate(&$form, &$form_state) {
  $ip_start = trim($form_state['values']['ip_start']);
  $ip_end = isset($form_state['values']['ip_end']) ? trim($form_state['values']['ip_end']) : FALSE;
  $own_ip = ($ip_start == ip_address() || ($ip_end && ip_ranges_check_range($ip_start . '-' . $ip_end, ip_address())));
  $black_flag = isset($form_state['values']['black_sign']) ? $form_state['values']['black_sign'] : 0;
  if (filter_var($ip_start, FILTER_VALIDATE_IP, FILTER_FLAG_NO_RES_RANGE) == FALSE) {
    form_set_error('ip_start', t("IP(/range start) is not a valid IP address."));
  }
  elseif ($ip_end && filter_var($ip_end, FILTER_VALIDATE_IP, FILTER_FLAG_NO_RES_RANGE) == FALSE) {
    form_set_error('ip_end', t("IP range end is not a valid IP address."));
  }
  elseif ($own_ip && $form_state['values']['type'] == 'blacklist' && !$black_flag) {
    // If user adds his/her own ip into Blacklist, we want him/her to confirm.
    drupal_set_message(t("You're trying to add your ip into blacklist! Please confirm before you proceed."), 'warning');
    $form_state['rebuild'] = TRUE;
    $form_state['flag'] = 1;
  }
}

/**
 * Form submission handler for domain_ip_form().
 */
function domain_ip_form_submit($form, &$form_state) {
  $ip_start = trim($form_state['values']['ip_start']);
  $ip_start = preg_replace('/\s+/', '', $ip_start);
  $author = trim($form_state['values']['author']);
  $ip = $ip_start;

  if ($form_state['values']['ip_end']) {
    $ip_end = trim($form_state['values']['ip_end']);
    $ip_end = preg_replace('/\s+/', '', $ip_end);
    $ip .= '-' . $ip_end;
  }

  $domain = $form_state['values']['domain'];
  $domain_id = $domain['domain_id'];

  $type = $form_state['values']['type'];
  db_insert('domain_ip')
    ->fields(array(
      'ip' => $ip,
      'type' => $type,
      'domain_id' => $domain_id,
      'author' => $author))
    ->execute();

  $list = ($type == 'blacklist') ? 'blacklisted' : 'whitelisted';
  drupal_set_message(t('The IP address %ip has been %list.', array('%ip' => $ip, '%list' => $list)));
  $form_state['redirect'] = 'admin/structure/domain/view/' . $domain_id . '/ip';
}

/**
 * IP deletion confirm page.
 *
 * @see domain_ip_delete_submit()
 */
function domain_ip_delete($form, &$form_state, $domain, $bid, $ip) {
  $form['ip_range'] = array(
    '#type' => 'value',
    '#value' => $bid,
  );
  $domain_id = $domain['domain_id'];
  $form['domain_id'] = array(
    '#type' => 'value',
    '#value' => $domain_id,
  );
  return confirm_form($form, t('Are you sure you want to delete ip/range %ip for domain %domain_name?',
    array(
      '%ip' => str_replace('-', ' - ', $ip),
      '%domain_name' => $domain['subdomain'])),
    'admin/structure/domain/view/' . $domain_id . '/ip',
    t('This action cannot be undone.'), t('Delete'), t('Cancel'));
}

/**
 * Processes domain_ip_delete form submissions.
 */
function domain_ip_delete_submit($form, &$form_state) {
  $ip = $form_state['build_info']['args']['2'];
  $bid = $form_state['values']['ip_range'];
  $domain_id = $form_state['values']['domain_id'];

  db_delete('domain_ip')
    ->condition('bid', $bid)
    ->execute();
  watchdog('user', 'Deleted %ip from banned ip-ranges', array('%ip' => $ip));
  drupal_set_message(t('The IP address/range %ip was deleted.', array(
    '%ip' => str_replace('-', ' - ', $ip))));
  $form_state['redirect'] = 'admin/structure/domain/view/' . $domain_id . '/ip';
}

/**
 * Store error message showed to banned user.
 *
 * @ingroup forms
 * @see domain_ip_error_form_submit()
 */
function domain_ip_error_form($form, $form_state) {
  $message = variable_get('Domain_IP_Banned_Error_Message', t('Sorry, you are banned for this site.'));
  $form['message'] = array(
    '#title' => t('Error message showed to banned user.'),
    '#type' => 'textarea',
    '#cols' => 48,
    '#rows' => 10,
    '#required' => TRUE,
    '#default_value' => $message,
    '#description' => t('This error message will be available for all domains.'),
  );
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('set'),
  );
  return $form;
}

/**
 * Form validation handler for domain_ip_error_form().
 */
function domain_ip_error_form_validate($form, &$form_state) {
  $message = trim($form_state['values']['message']);
  $len = drupal_strlen($message);
  if ($len > 10000) {
    form_set_error('message', t("Message could not exceed 10000 letters."));
  }
}

/**
 * Form submission handler for domain_ip_error_form().
 */
function domain_ip_error_form_submit($form, &$form_state) {
  $message = trim($form_state['values']['message']);
  variable_set('Domain_IP_Banned_Error_Message', $message);
  $form_state['redirect'] = 'admin/structure/domain/view/' . arg(4) . '/ip';
}

/**
 * Page Callback for admin/structure/domain/view/%domain/ip/whitelist_own.
 */
function domain_ip_whitelist_own_form($form, &$form_state, $domain) {
  global $user;
  $form['domain'] = array(
    '#type' => 'value',
    '#value' => $domain,
  );
  $form['author'] = array(
    '#title' => t('Your name'),
    '#default_value' => $user->name,
    '#type' => 'textfield',
    '#size' => 30,
    '#required' => TRUE,
    '#maxlength' => 25,
    '#description' => t('Please enter your name for your ip, so other administrators can recognize this ip.'),
  );
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Add'),
  );
  return $form;
}

/**
 * Submit handler for domain_ip_whitelist_own_form().
 */
function domain_ip_whitelist_own_form_submit($form, &$form_state) {
  $domain = $form_state['values']['domain'];
  $domain_id = $domain['domain_id'];
  $author = trim($form_state['values']['author']);
  domain_ip_write_record($domain_id, ip_address(), 'whitelist', $author);
  drupal_set_message(t('Your own IP-address @ip has been whitelisted.',
    array('@ip' => ip_address())));
  $form_state['redirect'] = 'admin/structure/domain/view/' . $domain_id . '/ip/';
}

/**
 * Switch ip list for a domain, e.g. switch from whitelist to blacklist.
 */
function domain_ip_switch_list($form, $form_state, $domain) {
  $domain_id = $domain['domain_id'];
  $domain_name = $domain['subdomain'];
  $list = domain_ip_get_domain_list($domain_id);
  if ($list == 'Blacklist' || $list == NULL) {
    $list = 'Whitelist';
    $question = t("You're switching to whitelist. Make sure you have
      whitelisted your own ip-address, or you will be banned 
      from %domain_name immediately!", array('%domain_name' => $domain_name));
  }
  else {
    $list = 'Blacklist';
    $question = t("You're switching to blacklist. All ips in blacklist will
      be banned, even if the ip is also in whitelist.");
  }
  $form['list'] = array(
    '#type' => 'value',
    '#value' => $list,
  );
  $form['domain_id'] = array(
    '#type' => 'value',
    '#value' => $domain_id,
  );
  return confirm_form($form,
    $question,
    'admin/structure/domain/view/' . $domain_id . '/ip/',
    t('You could always switch back.'),
    t('Confirm'),
    t('Cancel')
  );
}

/**
 * Submit handler for domain_ip_switch_list().
 */
function domain_ip_switch_list_submit($form, &$form_state) {
  $domain_id = $form_state['values']['domain_id'];
  $list = $form_state['values']['list'];
  domain_ip_list_write_record($domain_id, $list);
  drupal_set_message(t("Your have switched the list to @list.",
    array('@list' => $list)));
  $form_state['redirect'] = 'admin/structure/domain/view/' . $domain_id . '/ip/';
}
