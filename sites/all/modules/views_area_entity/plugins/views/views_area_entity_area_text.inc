<?php

/**
 * @file
 * A handler to provide an area that is coupled with the views area.
 *
 * @ingroup views_area_handlers
 */
class views_area_entity_area_text extends views_handler_area {

  /**
   * Implements views_handler_area#render().
   */
  function render($empty = FALSE) {
    if ($empty && $this->handler_type != 'empty' && empty($this->options['empty'])) {
      return '';
    }
    // This holds all entities for this view for all area types.
    $content = FALSE;
    if ($view_entity = $this->loadViewEntity($this->view->current_display)) {
      $content = entity_view('views_area_entity', array($view_entity->vaid => $view_entity), $this->handler_type);
    }
    $output = (string)$this->renderAreaText($content, $view_entity, $this->view->current_display);
    return $output;

  }

  function loadViewEntity($current_display = 'default') {
    $view = $this->view->name;
    if (module_exists('domain')) {
      $domain = domain_get_domain();
      if ($domain['domain_id'] != 0) {
        if ($entity = views_area_entity_get_entity($view, $current_display, $this->handler_type, $domain['domain_id'])) {
          return $entity;
        }
      }
    }
    if ($entity = views_area_entity_get_entity($view, $current_display, $this->handler_type)) {
      return $entity;
    }

    // Fallback to the default (master) display.
    if ($current_display != 'default') {
      return $this->loadViewEntity();
    }
    return FALSE;
  }

  function renderAreaText($content, $view_entity, $current_display) {
    $output = array();
    $output['#theme_wrappers'][] = 'views_area_entity_textarea';
    if ($view_entity && !empty($content['views_area_entity'][$view_entity->vaid])) {

      $output['content'] = $content['views_area_entity'][$view_entity->vaid];
      $output['#entity'] = $view_entity;
    }
    if (user_access('administer views area entities')) {
      $output['#contextual_links']['views_area_entity'] = array("admin/structure/views/area-entities/add/tmp/tmp/tmp", array());
      $output['#views_area_entity_contextual_links'] = array(
        'view' => $this->view->name,
        'current_display' => $this->view->current_display,
        'handler_type' => $this->handler_type
      );
      if (empty($output['content'])) {
        $output['content']['#markup'] = t('Admin only message: Use the contextual links to edit this area.');
      }
    }
    if (empty($output)) {
      return '';
    }
    return drupal_render($output);
  }

}
