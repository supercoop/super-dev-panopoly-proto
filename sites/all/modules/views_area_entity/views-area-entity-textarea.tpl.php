<?php
/**
 * @file
 * Themes the wrapper on the content returned by the views area entity.
 *
 * The role of $title_suffix is vital as this handles the contextual links.
 */
?>
<div class="<?php print $classes; ?>"<?php print $attributes; ?>>
<?php
  // Normally we have titles - not in this case! The contextual links
  // are appended here.
  print render($title_prefix) . render($title_suffix);
?>
  <div class="content"<?php print $content_attributes; ?>>
    <?php print $content ?>
  </div>
</div>