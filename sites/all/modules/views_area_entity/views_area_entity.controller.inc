<?php

/**
 * @file
 * Controller class definition file for the views area entity.
 */

/**
 * Views Area Entity Controller
 */
class ViewsAreaEntityController extends EntityAPIController {

  // The default controller doesn't provide a create() method, so add one.
  // This is a useful place to assign defaults to the properties.
  public function create(array $values = array()) {
    $values += array(
      'vaid' => NULL,
      'title' => '', // 255
      'view' => '', // 128
      'display' => '', // 64
      'type' => '', // either header, footer or empty
      'domain_id' => 0,
    );
    return parent::create($values);
  }

  // The default controller doesn't provide a save() method, so add one.
  public function save($area) {
    field_attach_presave('views_area_entity', $area);
    $area->changed = REQUEST_TIME;
    if (empty($area->vaid)) {
      $area->created = REQUEST_TIME;
      drupal_write_record('views_area_entity', $area);
      field_attach_insert('views_area_entity', $area);
      module_invoke_all('entity_insert', $area, 'views_area_entity');
      return SAVED_NEW;
    }
    else {
      drupal_write_record('views_area_entity', $area, array('vaid'));
      field_attach_update('views_area_entity', $area);
      module_invoke_all('entity_update', $area, 'views_area_entity');
      field_cache_clear();
      return SAVED_UPDATED;
    }
  }

  // The default controller doesn't provide a delete() method, so add one.
  public function delete($nids) {
    if (empty($nids)) {
      return FALSE;
    }

    $areas = entity_load('views_area_entity', $nids);
    db_delete('views_area_entity')
      ->condition('vaid', $nids, 'IN')
      ->execute();

    foreach ($areas as $area) {
      field_attach_delete('views_area_entity', $area);
      module_invoke_all('entity_delete', $area, 'views_area_entity');
    }

    cache_clear_all();
    $this->resetCache();
    return TRUE;
  }
}
