(function ($) {
  Drupal.behaviors.atheadliner_egp = {
    attach: function(context) {
      $('.four-4x25 .block-inner').equalHeight();
    }
  };
})(jQuery);