<?php
// AT Headliner

// Add some extra js if Superfish is enabled.
if (module_exists('superfish')) {
  drupal_add_js(drupal_get_path('theme', 'at_headliner') . '/js/sfnavbar.js',
    array(
      'type' => 'file',
      'scope' => 'header',
      'group' => JS_THEME,
      'preprocess' => TRUE,
      'cache' => TRUE,
    )
  );
}

/**
 * Override or insert variables into the html template.
 */
function at_headliner_preprocess_html(&$vars) {
  global $theme, $theme_key;

  // Add a class for the active color scheme
  if (module_exists('color')) {
    $class = check_plain(get_color_scheme_name($theme_key));
    $vars['classes_array'][] = 'color-scheme-' . drupal_html_class($class);
  }

  // Add class for the active theme
  $vars['classes_array'][] = drupal_html_class($theme_key);

  // Add conditional stylesheets
  drupal_add_css(path_to_theme() . '/css/ie/ie-lte-7.css', array(
    'group' => CSS_THEME,
    'browsers' => array(
      'IE' => 'lte IE 7',
      '!IE' => FALSE,
      ),
    'preprocess' => FALSE,
    )
  );
  drupal_add_css(path_to_theme() . '/css/ie/ie-lte-8.css', array(
    'group' => CSS_THEME,
    'browsers' => array(
      'IE' => 'lte IE 8',
      '!IE' => FALSE,
      ),
    'preprocess' => FALSE,
    )
  );
}

/**
 * Override or insert variables into the html template.
 */
function at_headliner_process_html(&$vars) {
  if (module_exists('color')) {
    _color_html_alter($vars);
  }
}


/**
 * Override or insert variables into the page template.
 */
function at_headliner_preprocess_page(&$vars) {
  // Add browser and platform classes
  $vars['classes_array'][] = css_browser_selector();

  $settings_array = array(
    'body_background',
    'header_layout',
    'menu_bullets',
  );
  foreach ($settings_array as $setting) {
    $vars['classes_array'][] = at_get_setting($setting);
  }
}


/**
 * Override or insert variables into the page template.
 */
function at_headliner_process_page(&$vars) {
  if (module_exists('color')) {
    _color_page_alter($vars);
  }
}

/**
 * Override or insert variables into the node template.
 */
function at_headliner_preprocess_node(&$vars) {
  if (variable_get('node_submitted_' . $vars['node']->type, TRUE)) {
    $vars['submitted'] = t('By !username on !datetime',
      array(
        '!username' => $vars['name'],
        '!datetime' => '<time datetime="' . $vars['datetime'] . '" pubdate="pubdate">' . $vars['date'] . '</time>',
      )
    );
  }
  else {
    $vars['submitted'] = '';
  }
}

/**
 * Override or insert variables into the block template.
 */
function at_headliner_preprocess_block(&$vars) {
  if ($vars['block']->module == 'superfish' || $vars['block']->module == 'nice_menu') {
    $vars['content_attributes_array']['class'][] = 'clearfix';
	  $vars['classes_array'][] = 'clearfix';
  }
  if (!$vars['block']->subject) {
    $vars['content_attributes_array']['class'][] = 'no-title';
  }
  if ($vars['block']->region == 'footer' || $vars['block']->region == 'menu_bar' || $vars['block']->region == 'menu_bar_top') {
    $vars['title_attributes_array']['class'][] = 'element-invisible';
  }
  if ($vars['block']->region == 'sub_menu_bar') {
    $vars['theme_hook_suggestions'][] = 'block__menu_bar';
    $vars['title_attributes_array']['class'][] = 'element-invisible';
  } 
  $er = block_list('sub_menu_bar');
  if ($vars['block']->region == 'menu_bar' && !empty($er)) {
    $vars['classes_array'][] = 'dark-arrow';
  }
}

/**
 * Override or insert variables into the field template.
 */
function at_headliner_preprocess_field(&$vars) {
  $element = $vars['element'];
  $vars['classes_array'][] = 'view-mode-'. $element['#view_mode'];
  $vars['image_caption_teaser'] = FALSE;
  $vars['image_caption_full'] = FALSE;
  if(theme_get_setting('image_caption_teaser') == 1) {
    $vars['image_caption_teaser'] = TRUE;
  }
  if(theme_get_setting('image_caption_full') == 1) {
    $vars['image_caption_full'] = TRUE;
  }
  $vars['field_view_mode'] = '';
  $vars['field_view_mode'] = $element['#view_mode'];
}

/**
 * Alter the search block form.
 */
function at_headliner_form_alter(&$form, &$form_state, $form_id) {
  if ($form_id == 'search_block_form') {
    $form['search_block_form']['#title'] = t('Search'); // Change the text on the label element
    $form['search_block_form']['#title_display'] = 'invisible'; // Toggle label visibilty
    $form['search_block_form']['#size'] = 25;  // define size of the textfield
    $form['search_block_form']['#attributes']['placeholder'] = t('Search'); // HTML5 placeholder attribute
  }
}

/**
 * Theme feed icons.
 */
function at_headliner_feed_icon($vars) {
  $text = t('Subscribe to @feed-title', array('@feed-title' => $vars['title']));
  $path = path_to_theme() . '/rss.png';
  if ($image = theme('image', array('path' => $path, 'alt' => $text))) {
    return l($image, $vars['url'], array('html' => TRUE, 'attributes' => array('class' => array('feed-icon'), 'title' => $text)));
  }
}
