<article class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>
  
  <div class="attribution">
    <?php print $picture; ?>
  </div>

  <div class="comment-text">
    <?php if ($new): ?>
      <span class="new"><?php print $new; ?></span>
    <?php endif; ?>
    <?php print render($title_prefix); ?>
      <header>
        <h3<?php print $title_attributes; ?>><?php print $title; ?></h3>
        <p class="comment-submitted">
        <?php
          print t('Submitted by !username on !datetime', array('!username' => $author, '!datetime' => '<time datetime="' . $datetime . '">' . $created . '</time>'));
        ?>
        </p>
      </header>
    <?php print render($title_suffix); ?>
    <div class="content"<?php print $content_attributes; ?>>
      <?php
        hide($content['links']);
        print render($content);
      ?>
      <?php if ($signature): ?>
        <div class="user-signature clearfix"><?php print $signature; ?></div>
      <?php endif; ?>
    </div>
    <?php print render($content['links']); ?>
  </div>

</article>
