<?php // Headliner ?>
<div id="page-wrapper">
	<div id="page" class="<?php print $classes; ?>">

		<?php if($page['menu_bar_top']): ?>
			<div id="menu-top-wrapper">
				<div class="container clearfix">
				  <?php print render($page['menu_bar_top']); ?>
			  </div>
			</div>
		<?php endif; ?>

		<div class="menu-top-border-bottom"></div>

		<div id="header-wrapper">
			<div class="container clearfix">
			  <header class="clearfix" role="banner">

					<?php if ($site_logo || $site_name || $site_slogan): ?>
						<!-- start: Branding -->
						<div id="branding" class="branding-elements clearfix">

							<?php if ($site_logo): ?>
								<div id="logo">
									<?php print $site_logo; ?>
								</div>
							<?php endif; ?>

							<?php if ($site_name || $site_slogan): ?>
								<!-- start: Site name and Slogan hgroup -->
								<hgroup id="name-and-slogan" <?php print $hgroup_attributes; ?>>

									<?php if ($site_name): ?>
										<h1 id="site-name" <?php print $site_name_attributes; ?>><?php print $site_name; ?></h1>
									<?php endif; ?>

									<?php if ($site_slogan): ?>
										<h2 id="site-slogan" <?php print $site_slogan_attributes; ?>><?php print $site_slogan; ?></h2>
									<?php endif; ?>

								</hgroup><!-- /end #name-and-slogan -->
							<?php endif; ?>

						</div><!-- /end #branding -->
					<?php endif; ?>

					<?php print render($page['header']); ?>

			  </header>
		  </div>
		</div>

		<div class="content-area-gradiant-top"></div>

		<?php if ($page['menu_bar'] || $page['sub_menu_bar']): ?>
			<div id="nav-wrapper">
				<div class="container clearfix">
				  <div class="menu-box">
						<?php if ($menubar = render($page['menu_bar'])): ?>
							<?php print $menubar; ?>
						<?php endif; ?>
						<?php if ($submenubar = render($page['sub_menu_bar'])): ?>
							<div class="menu-bottom">
								<?php print $submenubar; ?>
							</div>
						<?php endif; ?>
				  </div>
			  </div>
			</div>
		<?php endif; ?>

		<?php if ($messages || $page['help']): ?>
			<div id="messages-help-wrapper">
				<div class="container clearfix">
				  <?php print $messages; ?>
				  <?php print render($page['help']); ?>
			  </div>
			</div>
		<?php endif; ?>

		<!-- Two column 66-33 -->
		<?php if (
			$page['two_66_33_top'] ||
			$page['two_66_33_first'] ||
			$page['two_66_33_second'] ||
			$page['two_66_33_bottom']
			): ?>
			<div id="feature-wrapper">
				<div class="container">
				  <div class="at-panel gpanel panel-display two-66-33 clearfix">
					  <?php print render($page['two_66_33_top']); ?>
					  <?php print render($page['two_66_33_first']); ?>
					  <?php print render($page['two_66_33_second']); ?>
					  <?php print render($page['two_66_33_bottom']); ?>
				  </div>
			  </div>
			</div>
		<?php endif; ?>

		<!-- Two column 2x50 -->
		<?php if (
			$page['two_50_top'] ||
			$page['two_50_first'] ||
			$page['two_50_second'] ||
			$page['two_50_bottom']
			): ?>
			<div id="subfeatures-wrapper">
				<div class="container">
				  <div class="at-panel gpanel panel-display two-50 clearfix">
					  <?php print render($page['two_50_top']); ?>
					  <?php print render($page['two_50_first']); ?>
					  <?php print render($page['two_50_second']); ?>
					  <?php print render($page['two_50_bottom']); ?>
				  </div>
		    </div>
			</div>
		<?php endif; ?>

		<?php if ($page['secondary_content']): ?>
			<div id="secondary-content-wrapper">
				<div class="container clearfix">
				  <?php print render($page['secondary_content']); ?>
			  </div>
			</div>
		<?php endif; ?>

		<div id="content-wrapper">
			<div class="container">
			  <div id="columns">
					<div class="columns-inner clearfix">
				    <div id="content-column">
							<div class="content-inner">

								<?php print render($page['highlighted']); ?>

								<<?php print $tag; ?> id="main-content" role="main">
									<div class="inner">

										<?php if ($breadcrumb): ?>
											<div id="breadcrumb-wrapper" class="clearfix">
												<nav class="breadcrumb" role="navigation"><?php print $breadcrumb; ?></nav>
											</div>
										<?php endif; ?>

										<?php if ($title || $primary_local_tasks || $secondary_local_tasks || $action_links = render($action_links)): ?>
											<header class="clearfix">
												<?php print render($title_prefix); ?>
												<?php if ($title): ?>
													<h1 id="page-title"><?php print $title; ?></h1>
												<?php endif; ?>
												<?php print render($title_suffix); ?>

												<?php if ($primary_local_tasks || $secondary_local_tasks || $action_links): ?>
													<div id="tasks" class="clearfix" role="navigation">
														<?php if ($primary_local_tasks): ?>
															<ul class="tabs primary"><?php print render($primary_local_tasks); ?></ul>
														<?php endif; ?>
														<?php if ($secondary_local_tasks): ?>
															<ul class="tabs secondary"><?php print render($secondary_local_tasks); ?></ul>
														<?php endif; ?>
														<?php if ($action_links = render($action_links)): ?>
															<ul class="action-links"><?php print $action_links; ?></ul>
														<?php endif; ?>
													</div>
												<?php endif; ?>
											</header>
										<?php endif; ?>

										<div id="content">
											<?php print render($page['content']); ?>
										</div>

									</div>
								</<?php print $tag; ?>>

				      </div>
				    </div>

				    <?php print render($page['sidebar_first']); ?>
				    <?php print render($page['sidebar_second']); ?>

			    </div>
				</div>
		  </div>
		</div>

		<!-- Four column Gpanel -->
		<?php if (
			$page['four_first'] ||
			$page['four_second'] ||
			$page['four_third'] ||
			$page['four_fourth']
			): ?>
			<div id="quad-wrapper">
				<div class="container">
				  <div class="at-panel gpanel panel-display four-4x25 clearfix">
					  <div class="panel-row row-1 clearfix">
						  <?php print render($page['four_first']); ?>
						  <?php print render($page['four_second']); ?>
					  </div>
					  <div class="panel-row row-2 clearfix">
						  <?php print render($page['four_third']); ?>
						  <?php print render($page['four_fourth']); ?>
					  </div>
				  </div>
			  </div>
			</div>
		<?php endif; ?>

		<?php if ($page['tertiary_content']): ?>
			<div id="tertiary-content-wrapper">
				<div class="container clearfix">
				  <?php print render($page['tertiary_content']); ?>
			  </div>
			</div>
		<?php endif; ?>

		<!-- Three column 3x33 Gpanel -->
		<?php if (
			$page['three_33_top'] ||
			$page['three_33_first'] ||
			$page['three_33_second'] ||
			$page['three_33_third'] ||
			$page['three_33_bottom']
			): ?>
			<div class="footer-panels-border-top"></div>
			<div id="footer-panels-wrapper">
				<div class="container clearfix">
				  <div class="at-panel gpanel panel-display three-3x33 clearfix">
						<?php print render($page['three_33_top']); ?>
						<?php print render($page['three_33_first']); ?>
						<?php print render($page['three_33_second']); ?>
						<?php print render($page['three_33_third']); ?>
						<?php print render($page['three_33_bottom']); ?>
					</div>
				</div>
			</div>
		<?php endif; ?>

		<div class="footer-border-top"></div>
		<div id="footer-wrapper">
			<div class="container clearfix">
			  <?php if ($page['footer'] || $feed_icons): ?>
				  <footer class="clearfix" role="contentinfo">
					  <?php print render($page['footer']); ?>
					  <?php print $feed_icons; ?>
				  </footer>
			  <?php endif; ?>
		  </div>
		</div>

  </div>
</div>
