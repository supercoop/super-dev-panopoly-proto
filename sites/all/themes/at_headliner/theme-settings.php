<?php
// hook_form_system_theme_settings_alter()
function at_headliner_form_system_theme_settings_alter(&$form, &$form_state)  {

  // Page backgrounds
  $form['at']['pagestyles'] = array(
    '#type' => 'fieldset',
    '#title' => t('Page Styles'),
  );
  $form['at']['pagestyles']['body_background'] = array(
    '#type' => 'select',
    '#title' => t('Background overlays'),
    '#default_value' => theme_get_setting('body_background'),
    '#description' => t('This setting adds a texture or pattern over the main background color.'),
    '#options' => array(
      'bb-n'  => t('None'),
      'bb-h'  => t('Hatch'),
      'bb-vl' => t('Vertical lines'),
      'bb-hl' => t('Horizontal lines'),
      'bb-g'  => t('Grid'),
    ),
  );
 
  // Header layout
  $form['at']['header'] = array(
    '#type' => 'fieldset',
    '#title' => t('Header layout'),
  );
  $form['at']['header']['header_layout'] = array(
    '#type' => 'radios',
    '#title' => t('Branding position'),
    '#default_value' => theme_get_setting('header_layout'),
    '#description' => t('Change the position of the logo, site name and slogan. Note that his will automatically alter the header region layout also. If the branding elements are centered the header region will center below them, otherwise the header region will float in the opposite direction.'),
    '#options' => array(
      'hl-l'  => t('Left'),
      'hl-r'  => t('Right'),
      'hl-c' => t('Centered'),
    ),
  );
  $form['at']['menu_styles'] = array(
    '#type' => 'fieldset',
    '#title' => t('Menu Bullets'),
  );
  $form['at']['menu_styles']['menu_bullets'] = array(
    '#type' => 'select',
    '#title' => t('Menu Bullets'),
    '#default_value' => theme_get_setting('menu_bullets'),
    '#description' => t('Change the default menu bullets.'),
    '#options' => array(
      'mb-n'  => t('None'),
      'mb-dd' => t('Drupal default'),
      'mb-ah' => t('Arrow head'),
      'mb-ad' => t('Double arrow head'),
      'mb-ca' => t('Circle arrow'),
      'mb-fa' => t('Fat arrow'),
      'mb-sa' => t('Skinny arrow'),
    ),
  );
}
